package com.mall.user.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: dragon-mall
 * @description:
 * @author: Keyu Li
 * @create: 2021-12-13 09:27
 **/

@Data
public class UserGetLoginDto implements Serializable {
    private Long uid;
    private String file;
}
