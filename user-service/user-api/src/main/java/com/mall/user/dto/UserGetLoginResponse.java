package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

/**
 * @program: dragon-mall
 * @description:
 * @author: Keyu Li
 * @create: 2021-12-13 09:22
 **/

@Data
public class UserGetLoginResponse extends AbstractResponse {
    private UserGetLoginDto getLoginDto;
}
