package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;

/**
 * @program: dragon-mall
 * @description:
 * @author: Keyu Li
 * @create: 2021-12-13 09:51
 **/


public class UserLogoutResponse extends AbstractResponse {
}
