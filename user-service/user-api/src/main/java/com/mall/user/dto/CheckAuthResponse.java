package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.Map;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 22:29
 **/
@Data
public class CheckAuthResponse extends AbstractResponse {
    Map<String,Object> userinfo;
}
