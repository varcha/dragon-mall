package com.mall.user;

import com.mall.user.dto.*;

/**
 * @program: dragon-mall
 * @description:
 * @author: Keyu Li
 * @create: 2021-12-11 21:57
 **/

public interface IUserVerifyService {
    /*
    * 用户注册
    * */
    UserRegisterResponse registerUser(UserRegisterRequest quest);

    UserLoginResponse login(UserLoginRequest request);

    UserGetLoginResponse getLogin(UserGetLoginRequest request);

    UserLogoutResponse logout(UserLogoutRequest request);

    /*
    * 用户邮箱验证
    * */
    UserVerifyResponse verify(UserVerifyRequest request);

    CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest);
}
