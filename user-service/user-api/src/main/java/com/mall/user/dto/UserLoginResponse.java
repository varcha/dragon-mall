package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

/**
 * 日啊，为什么要把参数写在response里面啊！！！
 */
@Data
public class UserLoginResponse extends AbstractResponse {
//    private UserLoginDto loginDto;
    private Long id;
    private String username;
    private String phone;
    private String email;
    private String sex;
    private String address;
    private String file;
    private String description;
    private Integer points;
    private Long balance;
    private int state;
    private String token;
}
