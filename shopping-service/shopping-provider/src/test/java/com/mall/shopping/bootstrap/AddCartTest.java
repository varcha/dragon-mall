package com.mall.shopping.bootstrap;

import com.mall.shopping.ICartService;
import com.mall.shopping.dto.AddCartRequest;
import com.mall.shopping.dto.AddCartResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
//@ContextConfiguration
@RunWith(SpringRunner.class)
public class AddCartTest {

    @Reference(check = false,retries = 0)
    ICartService iCartService;

    @Test
    public void add1(){
        AddCartRequest request = new AddCartRequest();
        request.setUserId(111110L);
        request.setItemId(100023501L);
        request.setNum(5);
        System.out.println("add cart begin");
        iCartService.addToCart(request);
        System.out.println("test over");
    }


}