package com.mall.shopping.bootstrap;

import com.mall.shopping.converter.CategoriesConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dto.CategoriesDto;
import jdk.management.resource.ResourceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 14:40
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class MyTest1 {
    @Autowired
    CategoriesConverter categoriesConverter;
    @Autowired
    ItemCatMapper itemCatMapper;
    @Test
    public void test1(){
        ItemCat itemCat = new ItemCat();
        itemCat.setId(1L);
        itemCat.setIsParent(true);
        itemCat.setParentId(1L);
        CategoriesDto categoriesDto = categoriesConverter.categoriesDoToDto(itemCat);
        System.out.println(categoriesDto);
    }

    @Test
    public void test2(){
        List<ItemCat> itemCats = itemCatMapper.selectAll();
        System.out.println(itemCats);
    }
}
