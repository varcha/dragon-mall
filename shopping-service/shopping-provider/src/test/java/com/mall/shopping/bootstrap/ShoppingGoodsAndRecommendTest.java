package com.mall.shopping.bootstrap;

import com.mall.shopping.IProductService;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.AllProductResponse;
import com.mall.shopping.dto.RecommendResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName: ShoppingGoodsAndRecommendTest
 * @Description:
 * @author: PuJing
 * @create: 2021-12-13-16:02
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class ShoppingGoodsAndRecommendTest {

    @Autowired
    ItemMapper itemMapper;

    @Reference(check = false)
    IProductService iProductService;

    @Test
    public void testMapper() {
        Item item = itemMapper.selectByPrimaryKey(100023501);
        System.out.println(item);
    }

    @Test
    public void testService() {
        AllProductRequest request = new AllProductRequest();

        request.setPage(1);
        request.setSize(20);
        request.setCid(new Long(210));
        //request.setSort(String.valueOf(1));
        //request.setPriceGt(499);
        //request.setPriceLte(2000);

        AllProductResponse allProduct = iProductService.getAllProduct(request);
    }

    @Test
    public void testServiceRecommend() {

        RecommendResponse recommendGoods = iProductService.getRecommendGoods();
    }

}