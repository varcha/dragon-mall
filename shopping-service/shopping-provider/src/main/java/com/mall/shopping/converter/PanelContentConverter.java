package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dto.PanelContentDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PanelContentConverter  {
    PanelContentDto PanelContentToDto(PanelContentItem panelContent);

    List<PanelContentDto> PanelContentToDto(List<PanelContentItem> panelContent);


}
