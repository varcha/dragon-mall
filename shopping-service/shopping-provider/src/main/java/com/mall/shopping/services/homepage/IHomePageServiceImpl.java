package com.mall.shopping.services.homepage;

import com.mall.shopping.IHomePageService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.PanelContentItemConverter;
import com.mall.shopping.converter.PanelConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.PanelContentItemDto;
import com.mall.shopping.dto.PanelDetailDto;
import com.mall.shopping.dto.PanelDto;
import com.mall.shopping.dto.PanelResponse;
import javafx.scene.layout.Pane;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: dragon-mall
 * @description: 商品主页实现类
 * @author: zhu lei
 * @create: 2021-12-13 09:17
 **/
@Slf4j
@Service
@Component
public class IHomePageServiceImpl implements IHomePageService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    PanelConverter panelConverter;

    @Autowired
    PanelContentItemConverter panelContentItemConverter;

    @Override
    public PanelResponse getPanelResponse() {
        PanelResponse panelResponse = new PanelResponse();

        try {

//        从数据库里导出所有的panel数据，然后转成PanelDetailDto
            List<Panel> panels = panelMapper.selectAll();
            List<PanelDto> panelDtos = panelConverter.panelToDtoList(panels);
//        把一级数据里面的panel提取出来，根据panel找二级数据
            for (PanelDto panelDto : panelDtos) {
                //            此时找到了二级数据的内容，是数组型panelContentItems1
//                Example example = new Example(PanelContentItem.class);
//                example.createCriteria().andEqualTo("panel_id",panelDto.getId());
//                List<PanelContent> panelContentItems = panelContentMapper.selectByExample(example);
                List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentItemsByProductId((long)panelDto.getId());
//            但是还缺三个数据没赋值（在item表里面），需要根据二级数据里面的productId在item数据库找，然后遍历赋值
                for (PanelContentItem panelContentItem : panelContentItems) {
                    Item item = itemMapper.selectByPrimaryKey(panelContentItem.getProductId());
                    //Item item = itemMapper.selectItemByProductId(panelContentItem.getProductId());
//                项目表对应的title是panelContent表对应的产品名字,sellPoint对应的是subTitle
                    if (item==null){
                        continue;
                    }
                    panelContentItem.setProductName(item.getTitle());
                    panelContentItem.setSalePrice(item.getPrice());
                    panelContentItem.setSubTitle(item.getSellPoint());
                }
//            在这里要把PanelContentItem转换成PanelContentItemDto,这两个类实际上就是一模一样的
                List<PanelContentItemDto> panelConnectItemDtos = panelContentItemConverter.panelContentItemToDto(panelContentItems);
//            此时就可以给一级目录给panelConnectItems赋值
                panelDto.setPanelContentItems( panelConnectItemDtos);
//        最后再由panelDetail赋值给panelResponse
            }
            panelResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            panelResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            panelResponse.setPanelDtos( panelDtos);
        } catch (Exception e) {
            e.printStackTrace();
            panelResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            panelResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return panelResponse;
    }
}