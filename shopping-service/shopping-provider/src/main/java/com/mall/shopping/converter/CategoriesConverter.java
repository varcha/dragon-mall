package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dto.CategoriesDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CategoriesConverter {

    @Mappings({
            @Mapping(source = "icon" ,target = "iconUrl")
    })
    CategoriesDto categoriesDoToDto(ItemCat itemCat);
}
