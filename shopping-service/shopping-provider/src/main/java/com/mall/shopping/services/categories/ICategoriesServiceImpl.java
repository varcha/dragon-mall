package com.mall.shopping.services.categories;

import com.mall.shopping.ICategoriesService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.CategoriesConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dto.CategoriesDto;
import com.mall.shopping.dto.CategoriesResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: zengyi 305905917@qq.com
 * @create: 2021-12-13 09:45
 **/
@Service
@Component
@Slf4j
public class ICategoriesServiceImpl implements ICategoriesService {

    @Autowired
    ItemCatMapper itemCatMapper;

    @Autowired
    CategoriesConverter converter;

    @Override
    public CategoriesResponse getCategories() {

        CategoriesResponse categoriesResponse = new CategoriesResponse();

        try {
            //查询所有商品种类
            List<ItemCat> itemCats = itemCatMapper.selectAll();

            List<CategoriesDto> categoriesDtoList = new ArrayList<>();

            //遍历，然后一个个转化为Dto对象
            for (ItemCat itemCat : itemCats) {
                CategoriesDto categoriesDto = converter.categoriesDoToDto(itemCat);
                categoriesDtoList.add(categoriesDto);
            }
            categoriesResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            categoriesResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            categoriesResponse.setCategoriesDto(categoriesDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            categoriesResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            categoriesResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        //返回Dto的list
        return categoriesResponse;
    }
}
