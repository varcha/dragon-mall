package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dto.PanelDetailDto;
import com.mall.shopping.dto.PanelDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PanelConverter {

    PanelDto panelToDto(Panel panel);

    List<PanelDto> panelToDtoList(List<Panel> panel);
}
