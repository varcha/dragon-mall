package com.mall.shopping.dal.entitys;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 */
@Data
@Table(name = "tb_panel_content_item")
public class PanelContentItem {

    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer id;

    @Column(name = "panel_id")
    private Integer panelId;

    private Integer type;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "sort_order")
    private Integer sortOrder;

    @Column(name = "full_url")
    private String fullUrl;

    @Column(name = "pic_url")
    private String picUrl;

    @Column(name = "pic_url2")
    private String picUrl2;

    @Column(name = "pic_url3")
    private String picUrl3;

    private Date created;

    private Date updated;

    private String productName;

    private BigDecimal salePrice;

    private String subTitle;
}
