package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dto.PanelContentItemDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PanelContentItemConverter {

    List<PanelContentItemDto> panelContentItemToDto(List<PanelContentItem> panelContentItem);
}
