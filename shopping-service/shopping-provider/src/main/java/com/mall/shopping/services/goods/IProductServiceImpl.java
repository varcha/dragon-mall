package com.mall.shopping.services.goods;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.converter.ProductDetailConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.ItemDesc;
import com.mall.shopping.dal.persistence.ItemDescMapper;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.*;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @ClassName: IProductServiceImpl
 * @Description: 商品模块
 * @author: PuJing
 * @create: 2021-12-13-14:46
 **/
@Service
public class IProductServiceImpl implements IProductService {

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ItemMapper itemMapper;
    @Autowired
    ItemDescMapper itemDescMapper;

    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    ProductConverter converter;

    @Autowired
    ProductDetailConverter detailConverter;

    @Autowired
    ContentConverter contentConverter;

    @Override
    public ProductDetailResponse getProductDetail(ProductDetailRequest request) {
        ProductDetailResponse productDetailResponse = new ProductDetailResponse();
        ProductDetailDto productDetailDto = null;

        try {
            request.requestCheck();
            Item item = itemMapper.selectByPrimaryKey(request.getId());
            ItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(request.getId());

//            productDetailDto = detailConverter.productDescDoToDto(itemDesc);

            productDetailDto = detailConverter.productItemCatDoToDto(item,itemDesc);

            productDetailResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            productDetailResponse.setProductDetailDto(productDetailDto);
        } catch (Exception e) {
            e.printStackTrace();
            productDetailResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return productDetailResponse;

    }

    @Override
    public AllProductResponse getAllProduct(AllProductRequest request) {

        AllProductResponse response = new AllProductResponse();

        try {

            // 分页
            PageHelper.startPage(request.getPage(), request.getSize());

            request.requestCheck();

            Example example = new Example(Item.class);

            example.createCriteria().andGreaterThanOrEqualTo("price",request.getPriceGt())
                    .andLessThanOrEqualTo("price",request.getPriceLte())
                    .andEqualTo("cid",request.getCid());
            // 检查是否需要排序
            if (!request.getSort().equals("")) {

                if (Integer.valueOf(request.getSort()) == -1) {
                    // 按照价格从高到低排序
                    example.setOrderByClause("price" + " " + "desc");
                } else {
                    // 按照价格从低到高排序
                    example.setOrderByClause("price" + " " + "asc");
                }
            } else {
                // 默认按照价格从低到高排序
                example.setOrderByClause("price" + " " + "asc");
            }

            List<Item> items = itemMapper.selectByExample(example);

            List<ProductDto> productDtos = converter.items2Dto(items);

            response.setProductDtoList(productDtos);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());

            PageInfo pageInfo = new PageInfo(items);
            response.setTotal(pageInfo.getTotal());

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return response;
    }

    @Override
    public RecommendResponse getRecommendGoods() {

        RecommendResponse response = new RecommendResponse();

        try {
            List<Panel> panels = panelMapper.selectPanelContentById(6);

            PanelDto panelDto = contentConverter.panel2Dto(panels.get(0));



            List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(6);

            List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);

            panelDto.setPanelContentItems(panelContentItemDtos);

            Set<PanelDto> panelDtos = new HashSet<>();
            panelDtos.add(panelDto);

            response.setPanelContentItemDtos(panelDtos);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());

        } catch (Exception e){
            e.printStackTrace();
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }
}
