package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.entitys.ItemDesc;
import com.mall.shopping.dto.ProductDetailDto;
import com.mall.shopping.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductDetailConverter {

    @Mappings({
            @Mapping(source = "item.id" ,target = "productId"),
            @Mapping(source = "item.price",target = "salePrice"),
            @Mapping(source = "item.title",target = "productName"),
            @Mapping(source = "item.sellPoint",target = "subTitle"),
            @Mapping(source = "item.imageBig",target = "productImageBig"),
            @Mapping(source = "itemDesc.itemDesc",target = "detail"),
            @Mapping(target = "productImageSmall",expression = "java(getImages(item))"),

    })
    ProductDetailDto productItemCatDoToDto(Item item,ItemDesc itemDesc);
//    @Mappings({
//            @Mapping(source = "itemDesc",target = "detail")
//    })
//    ProductDetailDto productDescDoToDto(ItemDesc itemDesc);
    default List<String> getImages(Item item){
        String[] images = item.getImages();
        List<String> imageList = new ArrayList<>();
        for (String image : images) {
            imageList.add(image);
        }
        return imageList;
    }
}
