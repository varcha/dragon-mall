package com.mall.shopping.services.navigation;

import com.mall.shopping.INavigationService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.PanelContentConverter;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelContentDto;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.mall.shopping.constant.GlobalConstants.HEADER_PANEL_ID;

/**
 * @program: dragon-mall
 * @description: 导航栏实现类
 * @author: zhu lei
 * @create: 2021-12-13 21:12
 **/
@Slf4j
@Component
@Service
public class INavigationServiceImpl implements INavigationService {
    @Autowired
    PanelContentMapper panelContentMapper;
    @Autowired
    PanelContentConverter panelContentConverter;
    @Override
    public NavListResponse getNavList() {
        NavListResponse navListResponse = new NavListResponse();
        try {
            List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(HEADER_PANEL_ID);
            List<PanelContentDto> panelContentDtos = panelContentConverter.PanelContentToDto(panelContentItems);
            navListResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            navListResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            navListResponse.setPannelContentDtos(panelContentDtos);
        } catch (Exception e) {
            e.printStackTrace();
            navListResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            navListResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return navListResponse;
    }
}