package com.mall.shopping.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 09:35
 **/

@Data
public class CategoriesDto implements Serializable {
    private Long id;
    private String name;
    private Long parentId;
    private Boolean isParent;
    private String iconUrl;

}
