package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: zhu lei
 * @create: 2021-12-13 09:33
 **/
@Data
public class PanelResponse extends AbstractResponse {
    private List<PanelDto> panelDtos;

}