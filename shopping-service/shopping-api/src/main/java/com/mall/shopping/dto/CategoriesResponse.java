package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 10:44
 **/
@Data
public class CategoriesResponse extends AbstractResponse {
    List<CategoriesDto> categoriesDto;

}
