package com.mall.shopping.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: AllProductVO
 * @Description:
 * @author: PuJing
 * @create: 2021-12-13-22:40
 **/
@Data
public class AllProductVO {

    private List<ProductDto> data;

    private Long total;
}
