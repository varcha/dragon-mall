package com.mall.shopping;

import com.mall.shopping.dto.TestProductDetailDto;
import com.mall.shopping.dto.TestProductDetailRequest;
import com.mall.shopping.dto.TestProductDetailResponse;

public interface ITestProductDetailService {
    //TestProductDetailDto getProductDetail(Long productId);
    TestProductDetailResponse getProductDetail(TestProductDetailRequest request);
}
