package com.mall.shopping;

import com.mall.shopping.dto.NavListResponse;

public interface INavigationService {
    NavListResponse getNavList();
}
