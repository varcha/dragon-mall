package com.mall.shopping.dto;

import lombok.Data;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: zhu lei
 * @create: 2021-12-13 11:10
 **/
@Data
public class PanelDetailDto implements Serializable {
    private static final long serialVersionUID = -9099372701554072936L;
    private Integer id;

    private String name;

    private Integer type;

    private Integer sortOrder;

    private Integer position;

    private Integer limitNum;

    private Integer status;

    private String remark;

    private List<PanelContentItemDto> panelContentItems;


    private Long productId;

    private Date timestamp;
}