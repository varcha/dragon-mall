package com.mall.shopping;

import com.mall.shopping.dto.PanelDto;
import com.mall.shopping.dto.PanelResponse;

import java.util.List;

public interface IHomePageService {
    PanelResponse getPanelResponse();
}
