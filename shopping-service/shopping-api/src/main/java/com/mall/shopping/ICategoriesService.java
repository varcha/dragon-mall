package com.mall.shopping;

import com.mall.shopping.dto.CategoriesDto;
import com.mall.shopping.dto.CategoriesResponse;

import java.util.List;

public interface ICategoriesService {

//    List<CategoriesDto> getCategories();
    CategoriesResponse getCategories();

}
