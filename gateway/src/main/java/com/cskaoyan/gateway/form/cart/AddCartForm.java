package com.cskaoyan.gateway.form.cart;

import com.mall.shopping.dto.CartProductDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 添加购物车的前端传来的JSON字符串：
 *      {"userId":"62","productId":100053202,"productNum":1}
 */

@Data
public class AddCartForm {
    private Long userId;
    private Long productId;
    private Integer productNum;
}