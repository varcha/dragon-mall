package com.cskaoyan.gateway.form.cart;

import lombok.Data;

@Data
public class CheckItemAllForm {
    Long userId;
    String checked;
}