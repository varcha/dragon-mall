package com.cskaoyan.gateway.form.user;

import lombok.Data;

/**
 * @program: dragon-mall
 * @description: 接收 register 传来的参数
 * @author: Keyu Li
 * @create: 2021-12-11 21:37
 **/

@Data
public class RegisterForm {
    String userName;
    String userPwd;
    String email;
    String captcha;
}
