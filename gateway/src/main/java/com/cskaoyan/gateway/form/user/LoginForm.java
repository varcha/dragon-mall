package com.cskaoyan.gateway.form.user;

import lombok.Data;

/**
 * @program: dragon-mall
 * @description:
 * @author: Keyu Li
 * @create: 2021-12-12 22:47
 **/

@Data
public class LoginForm {
    String userName;
    String userPwd;
    String captcha;
}
