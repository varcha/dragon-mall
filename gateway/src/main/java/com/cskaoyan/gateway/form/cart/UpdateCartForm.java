package com.cskaoyan.gateway.form.cart;

import lombok.Data;

@Data
public class UpdateCartForm {
    private Long userId;
    private Long productId;
    private Integer productNum;
    private String checked;
}