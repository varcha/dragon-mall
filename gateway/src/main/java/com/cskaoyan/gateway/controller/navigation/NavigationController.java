package com.cskaoyan.gateway.controller.navigation;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.INavigationService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.NavListResponse;
import com.mall.user.annotation.Anoymous;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: dragon-mall
 * @description: 导航栏
 * @author: zhu lei
 * @create: 2021-12-13 20:45
 **/
@Slf4j
@RestController
@RequestMapping("/shopping")
public class NavigationController {
    @Reference(timeout = 3000,retries = 0,check = false)
    INavigationService iNavigationService;

    @GetMapping("/navigation")
    @Anoymous
    public ResponseData getNavList() {
        NavListResponse navList = iNavigationService.getNavList();
        if (!ShoppingRetCode.SUCCESS.getCode().equals(navList.getCode())) {
            return new ResponseUtil().setErrorMsg(navList.getMsg());
        }
        return new ResponseUtil().setData(navList.getPannelContentDtos());
    }

}