package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.result.ResultData;
import com.mall.order.OrderCoreService;

import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.*;
import com.mall.user.IMemberService;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.UUID;

/**
 * @description: 订单
 * @author: Mr.Jiang
 * @create: 2021-12-12 22:18
 **/
@RestController
@RequestMapping("shopping")

public class OrderController {

    @Reference(timeout = 3000, retries = 0, check = false)
    private OrderCoreService orderCoreService;

    @Reference(timeout = 3000, retries = 0, check = false)
    private OrderQueryService orderQueryService;

    @Reference(timeout = 3000, retries = 0, check = false)
    private IMemberService iMemberService;

    @PostMapping("order")
    public ResponseData order(@RequestBody CreateOrderRequest request, HttpServletRequest httpServletRequest) {
        // 获得session中的userinfo用户的登录信息，在TokenIntercepter放入seesion中的
        //设置uid
        HashMap<String, Object> userInfo = (HashMap<String, Object>) httpServletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        long uid = (long) userInfo.get("uid");
        request.setUserId(uid);
        //设置uniqueKey,通过uuid设置
        String uniqueKey = UUID.randomUUID().toString();
        request.setUniqueKey(uniqueKey);
        CreateOrderResponse order = orderCoreService.createOrder(request);
        if (order.getCode().equals(OrderRetCode.SUCCESS.getCode())) {
            ResponseData<Object> responseData = new ResponseUtil<>().setData(order.getOrderId());
            return responseData;
        }
        ResponseData<Object> objectResponseData = new ResponseUtil<>().setErrorMsg(order.getMsg());
        return objectResponseData;
    }


    @GetMapping("order")
    public ResponseData order(OrderListRequest request, HttpServletRequest httpServletRequest) {
        HashMap<String, Object> userInfo = (HashMap<String, Object>) httpServletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        long uid = (long) userInfo.get("uid");
        request.setUserId(uid);
        OrderListResponse orderListResponse = orderQueryService.order(request);
        ResultData resultData = new ResultData();
        resultData.setData(orderListResponse.getData());
        resultData.setTotal(orderListResponse.getTotal());
        if (orderListResponse.getCode().equals(OrderRetCode.SUCCESS.getCode())) {
            ResponseData<Object> responseData = new ResponseUtil<>().setData(resultData);
            return responseData;
        }
        ResponseData<Object> objectResponseData = new ResponseUtil<>().setErrorMsg(orderListResponse.getMsg());
        return objectResponseData;
    }

    @GetMapping("order/{id}")
    public ResponseData order(@PathVariable("id") String id, HttpServletRequest httpServletRequest) {
        HashMap<String, Object> userInfo = (HashMap<String, Object>) httpServletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        long uid = (long) userInfo.get("uid");
        String userName = (String) userInfo.get("userName");
        MyOrderDetailResponse myOrderDetailResponse = orderQueryService.detailOrder(id, uid, userName);
        if (myOrderDetailResponse.getCode().equals(OrderRetCode.SUCCESS.getCode())) {
            ResponseData<Object> responseData = new ResponseUtil<>().setData(myOrderDetailResponse);
            return responseData;
        }
        ResponseData<Object> objectResponseData = new ResponseUtil<>().setErrorMsg(myOrderDetailResponse.getMsg());
        return objectResponseData;
    }

    @PostMapping("cancelOrder")
    public ResponseData cancelOrder(@RequestBody CancelOrderRequest cancelOrderRequest) {
        CancelOrderResponse cancelOrderResponse = orderCoreService.cancelOrder(cancelOrderRequest);
        if (cancelOrderResponse.getCode().equals(OrderRetCode.SUCCESS.getCode())) {
            ResponseData<Object> responseData = new ResponseUtil<>().setData(cancelOrderResponse);
            return responseData;
        }
        ResponseData<Object> objectResponseData = new ResponseUtil<>().setErrorMsg(cancelOrderResponse.getMsg());
        return objectResponseData;
    }


    @DeleteMapping("order/{id}")
    public ResponseData deleteOrder(@PathVariable("id") String id) {
        DeleteOrderResponse deleteOrderResponse = orderCoreService.deleteOrder(id);
        if (deleteOrderResponse.getCode().equals(OrderRetCode.SUCCESS.getCode())) {
            ResponseData<Object> responseData = new ResponseUtil<>().setData(deleteOrderResponse);
            return responseData;
        }
        ResponseData<Object> objectResponseData = new ResponseUtil<>().setErrorMsg(deleteOrderResponse.getMsg());
        return objectResponseData;
    }


}
