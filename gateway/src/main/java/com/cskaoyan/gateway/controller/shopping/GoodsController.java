package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.AllProductResponse;
import com.mall.shopping.dto.AllProductVO;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: GoodsController
 * @Description: 分页查询商品列表
 * @author: PuJing
 * @create: 2021-12-13-14:21
 **/
@RestController
@RequestMapping("shopping")
public class GoodsController {

    @Reference(check = false)
    IProductService iProductService;


    @Anoymous
    @GetMapping("goods")
    public ResponseData goods(AllProductRequest allProductRequest){

        AllProductResponse allProduct = iProductService.getAllProduct(allProductRequest);

        if(!ShoppingRetCode.SUCCESS.getCode().equals(allProduct.getCode())){

            return new ResponseUtil().setErrorMsg(allProduct.getMsg());
        }

        AllProductVO allProductVO = new AllProductVO();

        allProductVO.setData(allProduct.getProductDtoList());
        allProductVO.setTotal(allProduct.getTotal());

        return new ResponseUtil().setData(allProductVO);
    }




}
