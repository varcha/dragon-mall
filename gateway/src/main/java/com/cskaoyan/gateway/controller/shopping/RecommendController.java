package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.RecommendResponse;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: RecommendController
 * @Description: 查询推荐商品
 * @author: PuJing
 * @create: 2021-12-13-17:02
 **/
@RestController
@RequestMapping("shopping")
public class RecommendController {

    @Reference(check = false)
    IProductService iProductService;

    @Anoymous
    @GetMapping("recommend")
    public ResponseData recommend(){

        RecommendResponse response = iProductService.getRecommendGoods();

        if(!ShoppingRetCode.SUCCESS.getCode().equals(response.getCode())){

            return new ResponseUtil().setErrorMsg(response.getMsg());
        }

        return new ResponseUtil().setData(response.getPanelContentItemDtos());


    }
}
