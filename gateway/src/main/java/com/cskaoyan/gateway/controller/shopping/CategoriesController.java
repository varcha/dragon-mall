package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICategoriesService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.CategoriesDto;
import com.mall.shopping.dto.CategoriesResponse;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 09:27
 **/
@RestController
@RequestMapping("shopping")
public class CategoriesController {


    @Reference(timeout = 3000, retries = 0, check = false)
    ICategoriesService iCategoriesService;

    /**
     * 列举所有商品的种类
     *
     * @return
     */
    @Anoymous
    @GetMapping("categories")
    public ResponseData categories() {

        CategoriesResponse categoriesResponse  = iCategoriesService.getCategories();
        if (!ShoppingRetCode.SUCCESS.getCode().equals(categoriesResponse.getCode())){
            return new ResponseUtil().setErrorMsg(categoriesResponse.getMsg());
        }
        return new ResponseUtil().setData(categoriesResponse.getCategoriesDto());
    }

}
