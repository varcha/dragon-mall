package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.ProductDetailRequest;
import com.mall.shopping.dto.ProductDetailResponse;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: dragon-mall
 * @description:
 * @author: 305905917@qq.com
 * @create: 2021-12-13 15:16
 **/
@RestController
@RequestMapping("shopping")
public class ProductController {

    @Reference(check = false)
    IProductService iProductService;

    /**
     * 显示商品详情
     * @param id
     * @return
     */
    @Anoymous
    @GetMapping("product/{id}")
    public ResponseData product(@PathVariable("id") Long id) {
        ProductDetailRequest productDetailRequest = new ProductDetailRequest();
        productDetailRequest.setId(id);
        ProductDetailResponse productDetail = iProductService.getProductDetail(productDetailRequest);
        if (!ShoppingRetCode.SUCCESS.getCode().equals(productDetail.getCode())){
            return new ResponseUtil().setErrorMsg(productDetail.getMsg());
        }
        return new ResponseUtil().setData(productDetail.getProductDetailDto());


    }
}
