package com.cskaoyan.gateway.controller.homepage;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IHomePageService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.PanelResponse;
import com.mall.user.annotation.Anoymous;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: dragon-mall
 * @description: 主页显示
 * @author: zhu lei
 * @create: 2021-12-12 23:18
 **/
@Slf4j
@RestController
@RequestMapping("/shopping")
public class HomePageController {
    @Reference(timeout = 3000, retries = 0, check = false)
    IHomePageService iHomePageService;

    @GetMapping("/homepage")
    @Anoymous
    public ResponseData getHomePage() {
        PanelResponse panelResponse = iHomePageService.getPanelResponse();
        log.info("查询成功");
        if (!ShoppingRetCode.SUCCESS.getCode().equals(panelResponse.getCode())) {
            return new ResponseUtil().setErrorMsg(panelResponse.getMsg());
        }
        return new ResponseUtil().setData(panelResponse.getPanelDtos());
    }
}