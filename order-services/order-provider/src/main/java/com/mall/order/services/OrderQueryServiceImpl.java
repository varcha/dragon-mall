package com.mall.order.services;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.converter.OrderConverter;
import com.mall.order.dal.entitys.*;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dto.*;
import com.mall.order.utils.ExceptionProcessorUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.utils.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * cskaoyan
 */
@Slf4j
@Component
@Service
public class OrderQueryServiceImpl implements OrderQueryService {
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderShippingMapper orderShippingMapper;
    @Autowired
    OrderItemMapper orderItemMapper;

    @Autowired
    OrderConverter orderConverter;

    @Override
    public OrderListResponse order(OrderListRequest request) {
        OrderListResponse orderListResponse = new OrderListResponse();
        try {

            PageHelper.startPage(request.getPage(), request.getSize());
            //订单查询example
            Example orderExample = new Example(Order.class);
            orderExample.createCriteria().andEqualTo("userId", request.getUserId());
            orderExample.orderBy("updateTime").desc();
            List<Order> orders = orderMapper.selectByExample(orderExample);
            List<OrderDetailInfo> orderDetailInfoList = new ArrayList<>();
            for (Order order : orders) {
                OrderDetailInfo orderDetailInfo = orderConverter.order2detail(order);
                orderDetailInfoList.add(orderDetailInfo);
            }
//        orderDetailInfoList = orderConverter.orders2details(orders);
            //查每个订单的商品数量
            for (OrderDetailInfo orderDetailInfo : orderDetailInfoList) {
                //查数量example
                Example orderItemExample = new Example(OrderItem.class);
                orderItemExample.createCriteria().andEqualTo("orderId", orderDetailInfo.getOrderId());
                List<OrderItem> orderItems = orderItemMapper.selectByExample(orderItemExample);
                List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);
                orderDetailInfo.setOrderItemDto(orderItemDtos);
                //查地址example
                Example shippingExample = new Example(OrderShipping.class);
                shippingExample.createCriteria().andEqualTo("orderId", orderDetailInfo.getOrderId());
                List<OrderShipping> orderShippings = orderShippingMapper.selectByExample(shippingExample);
                OrderShipping orderShipping = orderShippings.get(0);
                OrderShippingDto orderShippingDto = orderConverter.shipping2dto(orderShipping);
                orderDetailInfo.setOrderShippingDto(orderShippingDto);
            }
            orderListResponse.setData(orderDetailInfoList);
            PageInfo pageInfo = new PageInfo(orderDetailInfoList);
            orderListResponse.setTotal(pageInfo.getTotal());
            orderListResponse.setCode(OrderRetCode.SUCCESS.getCode());
            orderListResponse.setMsg(OrderRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询所有订单 异常");
            ExceptionProcessorUtils.wrapperHandlerException(orderListResponse, e);
        }
        return orderListResponse;
    }

    @Override
    public MyOrderDetailResponse detailOrder(String orderId, long uid, String userName) {
        MyOrderDetailResponse myOrderDetailResponse = new MyOrderDetailResponse();
        try {

            Example orderExample = new Example(Order.class);
            orderExample.createCriteria().andEqualTo("userId", uid).andEqualTo("orderId", orderId);
            List<Order> orders = orderMapper.selectByExample(orderExample);
            Order order = orders.get(0);
            myOrderDetailResponse.setUserName(userName);
            myOrderDetailResponse.setUserId(uid);
            myOrderDetailResponse.setOrderTotal(order.getPayment());
            myOrderDetailResponse.setOrderStatus(order.getStatus());
            Example orderItemExample = new Example(OrderItem.class);
            orderItemExample.createCriteria().andEqualTo("orderId", orderId);
            List<OrderItem> orderItems = orderItemMapper.selectByExample(orderItemExample);
            List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);
            myOrderDetailResponse.setGoodsList(orderItemDtos);
            myOrderDetailResponse.setOrderStatus(order.getStatus());

            // 查询order-shipping
            Example example = new Example(OrderShipping.class);
            example.createCriteria().andEqualTo("orderId", order.getOrderId());
            List<OrderShipping> orderShippings = orderShippingMapper.selectByExample(example);
            OrderShipping orderShipping = orderShippings.get(0);
            myOrderDetailResponse.setStreetName(orderShipping.getReceiverAddress());
            myOrderDetailResponse.setTel(orderShipping.getReceiverPhone());
            myOrderDetailResponse.setCode(OrderRetCode.SUCCESS.getCode());
            myOrderDetailResponse.setMsg(OrderRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("显示订单详情 异常");
            ExceptionProcessorUtils.wrapperHandlerException(myOrderDetailResponse, e);
        }
        return myOrderDetailResponse;
    }
}
