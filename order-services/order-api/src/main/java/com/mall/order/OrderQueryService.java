package com.mall.order;

import com.mall.order.dto.*;

/**
 */
public interface OrderQueryService {


    OrderListResponse order(OrderListRequest request);

    MyOrderDetailResponse detailOrder(String orderId, long uid,String userName);
}
