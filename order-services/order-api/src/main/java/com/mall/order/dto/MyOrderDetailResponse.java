package com.mall.order.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description:
 * @author: Mr.Jiang
 * @create: 2021-12-13 20:20
 **/
@Data
public class MyOrderDetailResponse extends AbstractResponse {
    private String userName;
    private Integer orderStatus;
    private BigDecimal orderTotal;
    private long userId;
    private List<OrderItemDto> goodsList;
    private String tel;
    private String streetName;
}
