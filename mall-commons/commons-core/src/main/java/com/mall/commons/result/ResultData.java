package com.mall.commons.result;

/**
 * @description:
 * @author: Mr.Jiang
 * @create: 2021-12-14 10:04
 **/

public class ResultData {
    private Object data;
    private Long total;

    public ResultData() {
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public ResultData(Object data, Long total) {
        this.data = data;
        this.total = total;
    }
}
